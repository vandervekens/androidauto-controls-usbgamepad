#!/usr/bin/env python
# Python script that enables keyboard mapping for a usb gamepad
# Used at Crankshaft CarOS to navigate at Android Auto interface

import pygame
from pygame import locals
import time
import keyboard

pygame.init()
pygame.joystick.init() # main joystick device system

try:
    j = pygame.joystick.Joystick(0) # create a joystick instance
    j.init() # init instance
    print 'Enabled joystick: ' + j.get_name()
    joyName = j.get_name()
except pygame.error:
    print 'no joystick found.'

while True:
    for e in pygame.event.get(): # iterate over event stack

        if e.type == pygame.locals.JOYAXISMOTION: # Read Analog Joystick Axis
            x1 = j.get_axis(0) # Left Stick Axis X
            y1 = j.get_axis(1) # Left Stick Axis Y

            if x1 < 0:
                print("left")
                keyboard.press_and_release('1')

            if x1 > 0:
                print("right")
                keyboard.press_and_release('2')

            if y1 < 0:
                print("up")
                keyboard.press_and_release('up')

            if y1 > 0:
                print("down")
                keyboard.press_and_release('down')

        if e.type == pygame.locals.JOYBUTTONUP: # Read the buttons

            if e.button == 0:
                print("X")
                keyboard.press_and_release('P')

            if e.button == 1:
                print("A")
                keyboard.press_and_release('enter')

            if e.button == 2:
                print("B")
                keyboard.press_and_release('escape')
            if e.button == 3:
                print("Y")
                keyboard.press_and_release('B')

            if e.button == 4:
                print("LT")
                keyboard.press_and_release('left')

            if e.button == 5:
                print("RT")
                keyboard.press_and_release('right')

            if e.button == 8:
                print("SELECT")
                keyboard.press_and_release('M')
                time.sleep(1) # Voice control activation needs a timeout

            if e.button == 9:
                print("START")
                keyboard.press_and_release('H')